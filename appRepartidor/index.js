// server.js
const app = require('./app')
const { logger } = require('./services/logger')

// listening to server 3000
app.listen(5000, () => {
  var mesg = 'Servidor de Repartirdor Iniciado en el puerto 5000'
  logger.log({
    level: 'info',
    class: 'index',
    message: mesg
  })
})
