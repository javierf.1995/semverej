/**
 * Summary (Manejo de peticiones para aplicacion del Repartidor)
 *
 * Description. (Peticiones manejadas
 *                 Recibir pedido del restaurante
 *                 Informar estado del pedido al cliente
 *                 Marcar como entregado
 *               )
 *
 * @class
 * @author Jorge Veliz
 * @since  1.0.0
 */
const express = require('express')
const app = express()
const { logger } = require('./services/logger')
const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({
  extended: false
}))
app.use(bodyParser.json())
const fs = require('fs')
var pedidos = require('./pedido.json')

/**
 *
 * Description. (Peticion POST HTTP para añadir una orden del restaurante para entregar al cliente)
 *
 * @since      1.0.0
 * @param {String} name   Nombre del cliente
*/
app.post('/recibir', (req, res) => {
  var mesg = 'Orden de restaurante recibida'
  logger.log({
    level: 'info',
    class: 'index',
    message: mesg
  })
  var cliente = req.body
  cliente.estado = 'recibida del restaurante, voy en camino'
  pedidos.push(cliente)
  try {
    fs.writeFileSync('./pedido.json', JSON.stringify(pedidos))
    mesg = `Orden de Cliente ${cliente.name} recibida del restaurante`
    logger.log({
      level: 'info',
      class: 'index',
      message: mesg
    })
  } catch (err) {
    console.error(err)
  }
  res.status(200).end()
})

/**
 *
 * Description. (Peticion GET HTTP para informar al cliente el estado de su orden)
 *
 * @since      1.0.0
 * @param {String} name   Nombre del cliente
*/
app.get('/informe', (req, res) => {
  var mesg = 'Peticion de informe orden de cliente recibida'
  logger.log({
    level: 'info',
    class: 'index',
    message: mesg
  })
  let verificacion = false
  var cliente = req.body
  pedidos.forEach(element => {
    if (element.name === cliente.name) {
      mesg = `Peticion de orden de Cliente ${cliente.name} encontrada`
      logger.log({
        level: 'info',
        class: 'index',
        message: mesg
      })
      verificacion = true
      cambiarEstadoRepartidor()
      res.status(200).send(element.estado)
    }
  })
  if (!verificacion) {
    mesg = `Orden de Cliente ${cliente.name} no existe`
    logger.log({
      level: 'error',
      class: 'index',
      message: mesg
    })
    res.status(400).end()
  }
})

/**
 *
 * Description. (cambia el estado de las ordenes de voy en camino a entregadas al cliente)
 *
 * @since      1.0.0
 * @param {String} name   Nombre del cliente
*/
async function cambiarEstadoRepartidor () {
  setTimeout(() => {
    var mesg = 'Pedidos cambiando de voy en camino a entregados al cliente'
    logger.log({
      level: 'info',
      class: 'index',
      message: mesg
    })
    pedidos.forEach(element => {
      if (element.estado === 'recibida del restaurante, voy en camino') {
        element.estado = 'entregada al cliente'
      }
    })
    try {
      fs.writeFileSync('./pedido.json', JSON.stringify(pedidos))
    } catch (err) {
      console.error(err)
    }
  }, 10000)
}

module.exports = app
