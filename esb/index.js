/**
 * Summary (ESB para la orquestación de servicios para aplicacion de Restaurante)
 *
 * Description. (Peticiones manejadas
 *                 Recibir pedido del cliente - enviar al restaurante
 *                 Solicitar estado Restaurante - Informar estado del pedido al cliente
 *                 Solicitar estado Repartidor - Informar estado del pedido al cliente
 *                 Pedido listo de Restaurante al repartidor
 *               )
 *
 * @class
 * @author Jorge Veliz
 * @since  1.0.0
 */
var request = require('request')
const express = require('express')
const app = express()
const { logger } = require('./services/logger')
const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({
  extended: false
}))
app.use(bodyParser.json())

/**
 *
 * Description. (Peticion POST HTTP para añadir una orden del cliente al restaurante)
 *
 * @since      1.0.0
 * @param {String} name   Nombre del cliente
*/
app.post('/recibir', (req, res) => {
  var mesg = 'Peticion de cliente recibida en el ESB'
  logger.log({
    level: 'info',
    class: 'index',
    message: mesg
  })
  const cliente = req.body
  var options = {
    method: 'POST',
    url: 'http://localhost:4000/recibir',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ name: cliente.name, estado: cliente.estado })
  }
  request(options, function (error, response) {
    let mesg = ''
    if (error) {
      mesg = `Peticion de pedido de Cliente ${cliente.name} erronea, respuesta del Restaurante`
      logger.log({
        level: 'error',
        class: 'index',
        message: mesg
      })
      res.status(400).end()
    }
    mesg = `Pedido de Cliente ${cliente.name} exitoso, respuesta del Restaurante`
    logger.log({
      level: 'info',
      class: 'index',
      message: mesg
    })
    res.status(200).end()
  })
})

/**
 *
 * Description. (Peticion GET HTTP para informar al repartidor que la orden esta lista)
 *
 * @since      1.0.0
 * @param {String} name   Nombre del cliente
*/
app.get('/informeRestaurante', (req, res) => {
  var mesg = 'Peticion de informe orden de cliente recibida en el ESB'
  logger.log({
    level: 'info',
    class: 'index',
    message: mesg
  })
  const cliente = req.body
  var options = {
    method: 'GET',
    url: 'http://localhost:4000/informe',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ name: cliente.name })
  }
  request(options, function (error, response) {
    let mesg = ''
    if (error) {
      mesg = `Peticion de informe de Cliente ${cliente.name} erronea, respuesta Restaurante`
      logger.log({
        level: 'error',
        class: 'index',
        message: mesg
      })
    }
    res.status(response.statusCode).send(response.body)
  })
})

/**
 *
 * Description. (Peticion GET HTTP para informar al cliente estado repartidor)
 *
 * @since      1.0.0
 * @param {String} name   Nombre del cliente
*/
app.get('/informeRepartidor', (req, res) => {
  var mesg = 'Peticion de informe orden de cliente recibida en el ESB'
  logger.log({
    level: 'info',
    class: 'index',
    message: mesg
  })
  const cliente = req.body
  var options = {
    method: 'GET',
    url: 'http://localhost:5000/informe',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ name: cliente.name })
  }
  request(options, function (error, response) {
    let mesg = ''
    if (error) {
      mesg = `Peticion de informe de Cliente ${cliente.name} erronea, respuesta Repartidor`
      logger.log({
        level: 'error',
        class: 'index',
        message: mesg
      })
    }
    mesg = 'Repartidor contesto al informe'
    logger.log({
      level: 'info',
      class: 'index',
      message: mesg
    })
    res.status(response.statusCode).send(response.body)
  })
})

app.post('/recibirRepartidor', (req, res) => {
  const element = req.body
  var mesg = 'Enviando del ESB al repartidor orden lista'
  logger.log({
    level: 'info',
    class: 'index',
    message: mesg
  })
  var options = {
    method: 'POST',
    url: 'http://localhost:5000/recibir',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ name: element.name, estado: element.estado })
  }
  request(options, function (error, response) {
    if (error) res.status(400).send()
    mesg = 'Orden del Restaurante al Repartidor por medio de ESB'
    logger.log({
      level: 'info',
      class: 'index',
      message: mesg
    })
    res.status(200).send()
  })
})

app.listen(6000, () => {
  var mesg = 'Servidor de Restaurante Iniciado en el puerto 6000'
  logger.log({
    level: 'info',
    class: 'index',
    message: mesg
  })
})
