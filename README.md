# Rama Master

Usada para representar lo ultimo entregado, los cambios hacia el otro release estan reflejados en esta rama

# Practica 7 - DEVOPS parte 3

aqui se configura un ciclo CI/CD usando las pipelines de gitlab para configurar las distintas stages
  > Build
  > Test
  > Release
en estas stage se pueden correr pruebas unitaras, revison de calidad de codigo y construccion de artefactos

### Link para descargar artefacto

https://gitlab.com/javierf.1995/semverej/-/jobs/artifacts/master/download?job=job_gulp



### Como Utilizar

Para trabajar en estos pipelines basta con hacer un merge/request o un commit en master.



