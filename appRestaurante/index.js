/**
 * Summary (Manejo de peticiones para aplicacion del Restaurante)
 *
 * Description. (Peticiones manejadas
 *                 Recibir pedido del cliente
 *                 Informar estado del pedido al cliente
 *                 Avisar al repartidor que ya está listo el pedido
 *               )
 *
 * @class
 * @author Jorge Veliz
 * @since  1.0.0
 */
var request = require('request')
const express = require('express')
const app = express()
const { logger } = require('./services/logger')
const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({
  extended: false
}))
app.use(bodyParser.json())
const fs = require('fs')
var pedidos = require('./pedido.json')

/**
 *
 * Description. (Peticion POST HTTP para añadir una orden del cliente)
 *
 * @since      1.0.0
 * @param {String} name   Nombre del cliente
*/
app.post('/recibir', (req, res) => {
  var mesg = 'Peticion de cliente recibida'
  logger.log({
    level: 'info',
    class: 'index',
    message: mesg
  })
  let verificacion = false
  var cliente = req.body
  pedidos.forEach(element => {
    if (element.name === cliente.name) {
      verificacion = true
    }
  })

  if (!verificacion) {
    pedidos.push(req.body)
    try {
      fs.writeFileSync('./pedido.json', JSON.stringify(pedidos))
      mesg = `Peticion de orden de Cliente ${cliente.name} añadida`
      logger.log({
        level: 'info',
        class: 'index',
        message: mesg
      })
    } catch (err) {
      console.error(err)
    }
    res.status(200).end()
  } else {
    mesg = `Peticion de orden de Cliente ${cliente.name} erronea`
    logger.log({
      level: 'error',
      class: 'index',
      message: mesg
    })
    res.status(400).end()
  }
})

/**
 *
 * Description. (Peticion GET HTTP para informar al cliente el estado de su orden)
 *
 * @since      1.0.0
 * @param {String} name   Nombre del cliente
*/
app.get('/informe', (req, res) => {
  cambiarEstadoCocinando()
  var mesg = 'Peticion de informe orden de cliente recibida'
  logger.log({
    level: 'info',
    class: 'index',
    message: mesg
  })
  let verificacion = false
  var cliente = req.body
  pedidos.forEach(element => {
    if (element.name === cliente.name) {
      mesg = `Peticion de orden de Cliente ${cliente.name} encontrada`
      logger.log({
        level: 'info',
        class: 'index',
        message: mesg
      })
      try {
        fs.writeFileSync('./pedido.json', JSON.stringify(pedidos))
      } catch (err) {
        console.error(err)
      }
      verificacion = true
      cambiarEstadoRepartidor()
      res.status(200).send(element.estado)
    }
  })
  if (!verificacion) {
    mesg = `Orden de Cliente ${cliente.name} no existe`
    logger.log({
      level: 'error',
      class: 'index',
      message: mesg
    })
    res.status(400).end()
  }
})

/**
 *
 * Description. (cambia el estado de las ordenes de recibidas a cocinando)
 *
 * @since      1.0.0
 * @param {String} name   Nombre del cliente
*/
function cambiarEstadoCocinando () {
  pedidos.forEach(element => {
    if (element.estado === 'enviado al restaurante') {
      element.estado = 'cocinando'
    }
  })
}

/**
 *
 * Description. (cambia el estado de las ordenes de cocinando a entregadas al repartidor)
 *
 * @since      1.0.0
 * @param {String} name   Nombre del cliente
*/
async function cambiarEstadoRepartidor () {
  setTimeout(() => {
    var mesg = 'Pedidos cambiando de cocinando a entregados al repartidor'
    logger.log({
      level: 'info',
      class: 'index',
      message: mesg
    })
    pedidos.forEach(element => {
      if (element.estado === 'cocinando') {
        element.estado = 'entregada al repartidor'

        var options = {
          method: 'POST',
          url: 'http://localhost:6000/recibirRepartidor',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({ name: element.name, estado: element.estado })
        }
        request(options, function (error, response) {
          if (error) throw new Error(error)
        })
      }
    })
    try {
      fs.writeFileSync('./pedido.json', JSON.stringify(pedidos))
    } catch (err) {
      console.error(err)
    }
  }, 10000)
}

app.listen(4000, () => {
  var mesg = 'Servidor de Restaurante Iniciado en el puerto 4000'
  logger.log({
    level: 'info',
    class: 'index',
    message: mesg
  })
})
