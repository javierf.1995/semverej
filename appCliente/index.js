/**
 * Summary (Manejo de peticiones para aplicacion del repartidor de Cliente)
 *
 * Description. (Peticiones manejadas
 *                 Solicitar pedido al restaurante
 *                 Verificar estado del pedido al restaurante
 *                 Verificar estado del pedido al repartidor
 *               )
 *
 * @class
 * @author Jorge Veliz
 * @since  1.0.0
 */
const { logger } = require('./services/logger')
var request = require('request')
const readLineSync = require('readline-sync')
/**
 *
 * Description. (Envia una orden al restaurante)
 *
 * @since      1.0.0
 * @param {String} nombre    nombre del cliente
*/
function solicitarPedidoRestaurante (nombre) {
  return new Promise((resolve, reject) => {
    var options = {
      method: 'POST',
      url: 'http://localhost:6000/recibir',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ name: nombre, estado: 'enviado al restaurante' })
    }
    request(options, function (error, response) {
      let mesg = ''
      if (error) {
        mesg = `Peticion de pedido de Cliente ${nombre} erronea`
        logger.log({
          level: 'error',
          class: 'index',
          message: mesg
        })
      }
      mesg = `Pedido de Cliente ${nombre} exitoso`
      logger.log({
        level: 'info',
        class: 'index',
        message: mesg
      })
      resolve(true)
    })
  })
}

/**
 *
 * Description. (recibe informe de estado de su orden)
 *
 * @since      1.0.0
 * @param {String} nombre    nombre del cliente
*/
function solicitarInformeRestaurante (nombre) {
  return new Promise((resolve, reject) => {
    var options = {
      method: 'GET',
      url: 'http://localhost:6000/informeRestaurante',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ name: nombre })
    }
    request(options, function (error, response) {
      let mesg = ''
      if (error) {
        mesg = `Peticion de informe de Cliente ${nombre} erronea`
        logger.log({
          level: 'error',
          class: 'index',
          message: mesg
        })
      }

      if (response.statusCode === 400) {
        mesg = `Usted ${nombre} no ha realizado ninguna orden al restaurante`
        logger.log({
          level: 'error',
          class: 'index',
          message: mesg
        })
      }

      mesg = `Su orden ${nombre} esta: ${response.body}`
      logger.log({
        level: 'info',
        class: 'index',
        message: mesg
      })
      resolve(true)
    })
  })
}

/**
 *
 * Description. (recibe informe de estado de su orden del repartidor)
 *
 * @since      1.0.0
 * @param {String} nombre    nombre del cliente
*/
function solicitarInformeRepartidor (nombre) {
  return new Promise((resolve, reject) => {
    var options = {
      method: 'GET',
      url: 'http://localhost:6000/informeRepartidor',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ name: nombre })
    }
    request(options, function (error, response) {
      let mesg = ''
      if (error) {
        mesg = `Peticion de informe de Cliente ${nombre} erronea`
        logger.log({
          level: 'error',
          class: 'index',
          message: mesg
        })
      }

      if (response.statusCode === 400) {
        mesg = `Usted ${nombre} no ha tiene ninguna orden con el repartidor`
        logger.log({
          level: 'error',
          class: 'index',
          message: mesg
        })
      }

      mesg = `Su orden ${nombre} esta: ${response.body}`
      logger.log({
        level: 'info',
        class: 'index',
        message: mesg
      })
      resolve(true)
    })
  })
}

async function imprimirMenu () {
  var menu = `opciones:
  1) Enviar pedido al Restaurante
  2) Solicitar informe Restaurante
  3) Solicitar informe Repartidor
  0) Salir`

  console.log(menu)
}

async function merequetengue () {
  let userRes = 9
  while (userRes !== '0') {
    const nombre = readLineSync.question('Escriba su nombre ')
    await imprimirMenu()
    userRes = readLineSync.question('Elija una opcion ')
    if (userRes === '1') {
      await solicitarPedidoRestaurante(nombre)
    } else if (userRes === '2') {
      await solicitarInformeRestaurante(nombre)
    } else if (userRes === '3') {
      await solicitarInformeRepartidor(nombre)
    }
  }
}

merequetengue()
